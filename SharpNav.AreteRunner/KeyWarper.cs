﻿using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.Interfaces;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreteRunner
{
    internal class KeyWarper
    {
        private KeyCache _keyCache;
        private Dictionary<string, List<uint>> _acgEntrances = new Dictionary<string, List<uint>>();

        private AutoResetInterval _tryEnterInterval = new AutoResetInterval(1000);
        private Queue<uint> _entrancesQueue = new Queue<uint>();
        private Identity _keySlot;
        private Identity _keyDynel;

        internal KeyWarper(string pluginDir)
        {
            LoadACGEntrances(pluginDir);
            _keyCache = new KeyCache(pluginDir);
        }

        private void LoadACGEntrances(string pluginDir)
        {
             _acgEntrances = JsonConvert.DeserializeObject<Dictionary<string, List<uint>>>(File.ReadAllText($"{pluginDir}\\ACGEntrances.json"));
        }

        internal void TryEnter(Identity keySlot, Identity keyDynelId)
        {
            if (_keyCache.TryGetValue(keyDynelId.Instance, out int cachedEntrance))
            {
                Item.UseItemOnItem(keySlot, new Identity(IdentityType.ACGEntrance, cachedEntrance));
                return;
            }

            if (_entrancesQueue.Any())
                return;

            string entranceName = N3EngineClientAnarchy.GetName(keyDynelId).Replace("Mission key to ", "");

            if (_acgEntrances.TryGetValue(entranceName, out List<uint> entrances)) 
            {
                _tryEnterInterval.Reset();
                _keySlot = keySlot;
                _keyDynel = keyDynelId;

                foreach (uint entrance in entrances)
                    _entrancesQueue.Enqueue(entrance);

                TryNextEntrance();
            }
        }

        internal void Update()
        {
            if (_tryEnterInterval.Elapsed && _entrancesQueue.Count > 0)
            {
                _entrancesQueue.Dequeue();

                if (_entrancesQueue.Count == 0)
                    return;

                TryNextEntrance();
            }
        }

        internal void TryNextEntrance()
        {
            if (!_entrancesQueue.Any())
                return;

            uint nextEntrance = _entrancesQueue.Peek();
            Item.UseItemOnItem(_keySlot, new Identity(IdentityType.ACGEntrance, unchecked((int)nextEntrance)));
        }

        internal void OnWrongMissionKey()
        {
            _entrancesQueue.Dequeue();
            _tryEnterInterval.Reset();
            TryNextEntrance();
        }

        internal void OnKeyAccepted()
        {
            if (!_entrancesQueue.Any())
                return;

            uint acceptedEntrance = _entrancesQueue.Dequeue();

            _entrancesQueue.Clear();

            _keyCache.AddKey(_keyDynel.Instance, unchecked((int)acceptedEntrance));
        }
    }

    internal class KeyCache
    {
        private Dictionary<int, int> _cachedKeys = new Dictionary<int, int>();
        private string _path;

        internal KeyCache(string pluginDir)
        {
            _path = $"{pluginDir}\\KeyCache.json";

            Load();
        }

        internal void AddKey(int dynelInst, int entranceInst)
        {
            _cachedKeys[dynelInst] = entranceInst;
            Save();
        }

        internal bool TryGetValue(int keyId, out int entranceId) => _cachedKeys.TryGetValue(keyId, out entranceId);

        private void Load()
        {
            if (!File.Exists(_path))
                return;

            _cachedKeys = JsonConvert.DeserializeObject<Dictionary<int, int>> (File.ReadAllText(_path));
        }

        private void Save()
        {
            File.WriteAllText(_path, JsonConvert.SerializeObject(_cachedKeys, Formatting.Indented));
        }
    }
}
