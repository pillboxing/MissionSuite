﻿using AOSharp.Core;
using AOSharp.Core.UI;

namespace AreteRunner
{
    public class Main : AOPluginEntry
    {

        public override void Run(string pluginDir)
        {
            Globals.PLUGIN_DIR = pluginDir;
            Chat.RegisterCommand("roller", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Chat.WriteLine("This character is now the roller.");
                BotAction.Roller = true;
            });
            Chat.RegisterCommand("missionrunner", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Chat.WriteLine("Mission runner kicking off!");
                BotNavigator.Initialize();
            });
            Chat.RegisterCommand("state", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Chat.WriteLine(BotAction.State);
            });
            Chat.RegisterCommand("location", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Chat.WriteLine(DynelManager.LocalPlayer.Position.ToString());
            });
            Chat.RegisterCommand("scanoutput", (string command, string[] param, ChatWindow chatWindow) =>
            {
                foreach (var item in BotNavigator.TeamTerminals[Playfield.ModelIdentity.Instance])
                {
                    Chat.WriteLine("TeamTerminals[" + Playfield.ModelIdentity.Instance.ToString() + "]" + ".Add(" + "new Vector3" + item.ToString() + ");");
                }
            foreach (var item in BotNavigator.SoloTerminals[Playfield.ModelIdentity.Instance])
            {
                Chat.WriteLine("SoloTerminals[" + Playfield.ModelIdentity.Instance.ToString() + "]" + ".Add(" + "new Vector3" + item.ToString() + ");");
                }
            });
            Chat.RegisterCommand("roll", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (param[0] == null)
                {
                    Chat.WriteLine("Please enter an item name to roll");
                }
                BotAction.RollList.Add(param[0]);
            });
        }
    }
}