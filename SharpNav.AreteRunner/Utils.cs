﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Linq;

namespace AreteRunner
{
    public class Utils
    {
        public static void KnubotStartTrade() => Network.Send(new KnuBotStartTradeMessage { Unknown1 = 2, Target = Targeting.TargetChar.Identity, Message = "" });

        public static void KnubotFinishTrade() => Network.Send(new KnuBotFinishTradeMessage { Unknown1 = 2, Amount = 15000, Target = Targeting.TargetChar.Identity });

        public static void UseDynel() => Network.Send(new GenericCmdMessage { Temp4 = 1, Unknown = 1, Count = 0xff, User = DynelManager.LocalPlayer.Identity, Action = GenericCmdAction.Use, Target = Targeting.Target.Identity });

        public static void UseLastItem() => Network.Send(new GenericCmdMessage { Unknown = 1, Count = 0xff, User = DynelManager.LocalPlayer.Identity, Action = GenericCmdAction.Use, Target = new Identity(IdentityType.Inventory, Globals.NEXT_FREE_INVENTORY_SLOT) });

        public static int GetNextAvailableSlot() => Enumerable.Range(0x40, 0x1E).Except(Inventory.Items.Where(x => x.Slot.Type == IdentityType.Inventory).Select(x => (x.Slot.Instance & 0xFF))).FirstOrDefault();

        public static bool UsedCreditCard() => new int[2] { Const.POST_CC_CASH, Const.POST_QUEST_CASH }.Contains(DynelManager.LocalPlayer.GetStat(Stat.Cash));

        public static void UpdatePosition()
        {
            Network.Send(new CharDCMoveMessage { Unknown = 1, Heading = DynelManager.LocalPlayer.Rotation, Position = DynelManager.LocalPlayer.Position, MoveType = MovementAction.SwitchToSit });
            Network.Send(new CharacterActionMessage() { Action = CharacterActionType.StandUp });
        }
    }
}