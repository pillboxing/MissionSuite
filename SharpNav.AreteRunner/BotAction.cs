﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Pathfinding;
using AOSharp.Core.UI;
using SharpNav;
using System.Linq;
using System;
using System.Runtime.CompilerServices;
using AOSharp.Core.Movement;
using System.Collections.Generic;
using Newtonsoft.Json.Schema;
using System.Reflection;
using AOSharp.Common.Unmanaged.Imports;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Diagnostics;

namespace AreteRunner
{
    public class BotAction
    {
        internal static String State = "Initialising";
        public static Dynel Target;
        static string Destination;
        static Mission SelectedMission;
        static bool Looter;
        public static bool Roller = false;
        static bool caller;
        static bool MapGenerated;
        static int CurrentFloor;
        static LocalPlayer Me = DynelManager.LocalPlayer;
        static bool IsFighting = false;
        public static MissionTerminal CurrentTerminal;
        public static bool MissionsRolled;
        static Dynel UpButton;
        static Dictionary<int,Vector3> DownButtons = new Dictionary<int, Vector3>();
        static Vector3 Exit;
        public static bool PendingTeleport;
        static Vector3 TargetButton = new Vector3(0,0,0);
        static String LeaderName;
        static List<Room> RoomsOfInterest = new List<Room>();
        public static Room TargetRoom;
        public static List<String> RollList = new List<String>();
        public static bool TerminalUsed;
        internal static void Init()
        {
            SMovementController.Set();
            SMovementController.AutoLoadNavmeshes($"c:/navmeshes");
           
          /*  MissionTerminal mishterm = new MissionTerminal(DynelManager.AllDynels.Find(x => x.Name.Contains("Mission Terminal")));
            mishterm.RequestMissions(3, 255, 122, 122, 122, 122, 0);
            mishterm.*/
        }

        internal static void OnMishesUpdated(RollListChangedArgs e)
        {
            
        }

        internal static void ProcessState()
        {

            switch (State)
            {
                case "Initialising":
                    {
                        if (Team.IsInTeam)
                        {
                            LeaderName = Team.Members.OrderBy(A => A.Name).First().Name;
                        }
                        if (!HaveMission() && !Playfield.IsDungeon)
                        {
                            State = "Rolling";
                        }
                        else
                        {
                            SelectedMission = Mission.List.First();
                            if (SelectedMission == null)
                            {
                                Chat.WriteLine("No missions found");
                            }
                            if (!Playfield.IsDungeon)
                            {
                                if (Playfield.Identity == SelectedMission.Location.Playfield)
                                {
                                    TeleportToMission();
                                }
                                else
                                {
                                    State = "Travelling";
                                    Chat.WriteLine("We are in a different zone to the mission, travelling to mission zone");
                                    Destination = "Mission";
                                }
                            }
                            else
                            {
                                CurrentFloor = DynelManager.LocalPlayer.Room.Floor;
                                Exit = DynelManager.LocalPlayer.Room.Doors.OrderBy(A => A.Position.DistanceFrom(DynelManager.LocalPlayer.Position)).First().Position;
                                Chat.WriteLine("We are in a dungeon, assume we're in a mission for now");
                                BotNavigator.NavGen();
                                RoomsOfInterest = Playfield.Rooms;
                                RoomsOfInterest.Remove(DynelManager.LocalPlayer.Room);
                                RoomsOfInterest.RemoveAll(A => A.Floor != DynelManager.LocalPlayer.Room.Floor);
                                State = "baking";
                            }
                        }
                        break;
                    }
                case "Travelling":
                    {
                        if (DynelManager.LocalPlayer.Room != TargetRoom)
                        {
                            return;
                        }
                        else
                        {
                            State = "Scanning";
                        }
                        break;
                    }
                case "NextRoom":
                    {
                        Vector3 ReferencePoint;
                        SimpleChar LeaderRef;
                        if (DynelManager.Find(LeaderName, out LeaderRef))
                        {
                            ReferencePoint = LeaderRef.Position;
                        }
                        else
                        {
                            ReferencePoint = DynelManager.LocalPlayer.Position;
                        }
                        var FloorRooms = RoomsOfInterest;
                        List<Room> PotentialRooms = new List<Room>();
                        if (FloorRooms.Count == 0)
                        {
                            State = "NextFloor";
                        }
                        else
                        {
                            FloorRooms.OrderBy(A => ReferencePoint.DistanceFrom(A.Center));
                            TargetRoom = FloorRooms.First();
                            SMovementController.SetNavDestination(TargetRoom.Position);
                            PotentialRooms.Clear();
                            State = "Travelling";
                        }

                        break;
                    }
                case "Scanning":
                    {
                        SMovementController.Halt();
                        List<SimpleChar> mobs = new List<SimpleChar>();
                        mobs = GetMobsInRoom(TargetRoom);

                        if (mobs.Count == 0)
                        {
                            RoomsOfInterest.Remove(TargetRoom);
                            TargetRoom = null;
                            Chat.WriteLine("Room cleared, looting");
                            State = "Looting";
                            break;
                        }
                        mobs.OrderBy(A => A.DistanceFrom(DynelManager.LocalPlayer));
                        DynelManager.LocalPlayer.Attack(mobs.First());
                        Chat.WriteLine("Engaging: " + mobs.First().Name);
                        mobs.First().Target();
                        SMovementController.SetNavDestination(mobs.First().Position);
                        Target = mobs.First();
                        State = "Fighting";
                            if (!GetUpButton().IsValid)
                            break;
                        if (TargetButton == new Vector3(0, 0, 0))
                        {
                            Chat.WriteLine("Found up button");
                            TargetButton = GetUpButton().Position;
                        }
                        break;
                    }
                case "Fighting":
                    {
                        if (!Target.IsValid  || (!DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending))
                        {
                            Target = null;
                            State = "Scanning";
                            break;
                        }
                        SimpleChar target = (SimpleChar)Target;
                        if (!target.IsInLineOfSight)
                        {
                            SMovementController.SetNavDestination(Target.Position);
                        }

                   
                        break;
                    }
                case "Looting":
                    {
                        var loot = GetLootInRoom(DynelManager.LocalPlayer.Room);
                        Chat.WriteLine("Found " + loot.Count + " lootable objects, but moving on for now!");
                        Chat.WriteLine("Room finished: " + RoomsOfInterest.Count.ToString() + " rooms to go.");
                        State = "NextRoom";
                        break;
                    }
                case "Rolling":
                    {
                        if (HaveMission())
                        {
                            State = "Initialising";
                        }
                        if (!Roller)
                            break;
                        if (SMovementController.IsNavigating())
                        {
                            if (DynelManager.LocalPlayer.Position.DistanceFrom(BotNavigator.FindClosestMissionPoint()) < 3)
                            {
                                SMovementController.Halt();
                            }
                        }
                        GoToTerminals();
                        RollMissions();
                        break;
                    }
                case "NextFloor":
                    {

                        if (DynelManager.LocalPlayer.Room.NumDoors == 0)
                        {
                            State = "CompleteMission";
                            break;
                        }

                        if (GetUpButton() == null)
                        {
                            SMovementController.SetNavDestination(TargetButton,true);
                            break;
                        }
                        if (!SMovementController.IsNavigating())
                        {
                            if (GetUpButton().DistanceFrom(DynelManager.LocalPlayer) < 2)
                            {
                                Chat.WriteLine("Using up button.");
                                SMovementController.Halt();
                                GetUpButton().Use();
                                TargetButton = new Vector3(0, 0, 0);
                                State = "GoingUp";
                                break;
                            }

                            SMovementController.SetNavDestination(TargetButton);
                        }
                        break;
                    }
                case "CompleteMission":
                    {
                        if (Mission.List.Count == 0)
                        {
                            Chat.WriteLine("Finished mission, exiting");
                            State = "ExitMission";
                            break;
                        }
                        if (!Roller)
                        {

                            break;
                        }
                        
                        foreach (MissionAction action in Mission.List.First().Actions)
                        {
                            switch (action.Type)
                            {
                                case MissionActionType.FindItem:
                                    Dynel findItemTarget = DynelManager.AllDynels.FirstOrDefault(x => x.Identity == ((FindItemAction)action).Target);

                                    if (findItemTarget == null)
                                    {
                                        Console.WriteLine("Could not find mission item");
                                        continue;
                                    }

                                    if (Roller)
                                    {
                                        findItemTarget.Target();
                                    }
                                    break;
                                case MissionActionType.FindPerson:
                                    Dynel findPersonTarget = DynelManager.AllDynels.FirstOrDefault(x => x.Identity == ((FindPersonAction)action).Target);

                                    if (findPersonTarget == null)
                                        continue;
                                    findPersonTarget.Target();
                                    break;
                                case MissionActionType.UseItemOnItem:

                                        Chat.WriteLine("Looking for mission item: " + Inventory.Items.Find(A=>A.Id == ((UseItemOnItemAction)action).Source.Instance));
                                        Item.UseItemOnItem(Inventory.Items.Find(A => A.Id == ((UseItemOnItemAction)action).Source.Instance).Slot, DynelManager.AllDynels.FirstOrDefault(x => x.Identity == ((UseItemOnItemAction)action).Destination).Identity);

                                    Dynel item2 = DynelManager.AllDynels.FirstOrDefault(x => x.Identity == ((UseItemOnItemAction)action).Destination);

                                    if (item2 != null)
                                    {
                                        Chat.WriteLine("Looking for mission item: " + Inventory.Items.Find(A => A.Id == ((UseItemOnItemAction)action).Source.Instance).Name);

                                    }

                                    break;
                            }
                        }
                        //Chat.WriteLine("Mission completed, exiting");
                        //State = "ExitMission";
                        break;
                    }
                case "ExitMission":
                    {
                        if (!Playfield.IsDungeon)
                        {
                            State = "Rolling";
                        }
                        if (GetDownButton() == null)
                        {
                            if (CurrentFloor != 0)
                            {

                                SMovementController.SetNavDestination(DownButtons[CurrentFloor],true);
                            }
                            else
                            {
                                SMovementController.SetNavDestination(Exit);
                                DownButtons.Clear();

                            }
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.DistanceFrom(GetDownButton()) < 5)
                            {
                                SMovementController.Halt();
                                GetDownButton().Use();
                                State = "GoingDown";
                                break;
                            }
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(GetDownButton().Position);
                            }
                        }
                        break;
                    }
                case "Baking":
                    {
                        Chat.WriteLine("Waiting on Navmesh");
                        if (SMovementController.IsLoaded())
                        {
                            State = "NextRoom";
                        }
                        break;
                    }
                case "GoingUp":
                    {
                        if (DynelManager.LocalPlayer.Room == null)
                        {
                            break;
                        }
   
                        if (DynelManager.LocalPlayer.Room.Floor > CurrentFloor)
                        {
                            var Button = GetDownButton();
                            if (Button != null)
                            {
                                DownButtons.Add(DynelManager.LocalPlayer.Room.Floor, Button.Position);
                                Chat.WriteLine("Found down button");
                            }

                            CurrentFloor = DynelManager.LocalPlayer.Room.Floor;
                            TargetRoom = DynelManager.LocalPlayer.Room;
                            State = "Scanning";
                            var temp = Playfield.Rooms;
                            temp.RemoveAll(A => A.Floor != DynelManager.LocalPlayer.Room.Floor);
                            RoomsOfInterest = temp;
                        }
                        break;
                    }
                case "GoingDown":
                    {
                        if (DynelManager.LocalPlayer.Room == null)
                        {
                            break;
                        }
                        if (DynelManager.LocalPlayer.Room.Floor < CurrentFloor)
                        {
  
                            CurrentFloor = DynelManager.LocalPlayer.Room.Floor;
                            State = "ExitMission";
                        }
                        break;
                    }
            }
        }

        private static List<SimpleChar> GetMobsInRoom(Room room)
        {
            var mobs = DynelManager.NPCs.ToList();
            if (mobs.Count == 0 || mobs == null)
            {
                return new List<SimpleChar>();
            }
            else
            {
                mobs.RemoveAll(A => A.Room != room);
                if (mobs.Count == 0)
                {
                    return new List<SimpleChar>();
                }
                else
                {
                    mobs.RemoveAll(A => !A.IsValid || A.Health <= 0);
                    return mobs;
                }
            }
        }
        private static List<Dynel> GetLootInRoom(Room room)
        {
            var dynels = DynelManager.AllDynels.ToList();
            if (dynels.Count == 0 || dynels == null)
            {
                return new List<Dynel>();
            }
            else
            {
                dynels.RemoveAll(d => d.Room != room && d.Name != "Treasure" && d.Name != "Bottles and Garbage" && d.Name != "Box" && d.Name != "Skeleton" && d.Name != "A Crashed Android" && d.Name != "Broken Machine" && d.Name != "Barrel" || d.Name.Contains("Remains"));
                if (dynels.Count == 0)
                {
                    return new List<Dynel>();
                }
                else
                {
                    dynels.RemoveAll(A => !A.IsValid);
                    return dynels;
                }
            }
        }

        private static void GoToTerminals()
        {
            Dynel term = DynelManager.AllDynels.Find(A => A.Name.Contains("Team Mission Terminal"));
            if (term != null)
            {
                if (term.DistanceFrom(DynelManager.LocalPlayer) < 5)
                {
                    return;
                }

            }


        }

        private static SimpleChar GetClosestMob()
        {
            Vector3 ReferencePosition;
            if (LeaderName != null)
            {
                var leader = DynelManager.Players.FirstOrDefault(A => A.Name == LeaderName);
                if (leader != null)
                {
                    ReferencePosition = leader.Position;
                }
                else
                {
                    ReferencePosition = DynelManager.LocalPlayer.Position;
                }
            }
            else
            {
                ReferencePosition = DynelManager.LocalPlayer.Position;
            }

            var Target1 = DynelManager.AllDynels
                    .Where(c => c.Name == "Alarm Sentry")
                    .OrderBy(c => c.Position.DistanceFrom(ReferencePosition))
                    .FirstOrDefault();
            var Target2 = DynelManager.NPCs
                .Where(c => c.Health > 0 && c.Name != "NoName")
                .OrderBy(c => c.Position.DistanceFrom(ReferencePosition))
                .ThenBy(c => c.HealthPercent)
                .FirstOrDefault();
            if (Target1 == null && Target2 != null)
            {
                return Target2;
            }
            if (Target2 == null && Target1 != null)
            {
                return (SimpleChar)Target1;
            }
            if (Target2 == null && Target1 == null)
            {
                return null;
            }

            if (Target1.Position.DistanceFrom(ReferencePosition) < Target.Position.DistanceFrom(ReferencePosition))
            {
                return (SimpleChar)Target1;   
            }
            else
            {
                return Target2;
            }

        }

        private static void TeleportToMission()
        {
            if (PendingTeleport)
                return;
            Chat.WriteLine("Teleporting to mission");
            SMovementController.UnloadNavmesh();
            PendingTeleport = true;
            Inventory.Items.FirstOrDefault(k => k.Name.Contains("Key to")).Use();
        }

        private static bool HaveMission()
        {
            var mish = Mission.List.FirstOrDefault();
            if (mish != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static List<Dynel> GetAllLoot()
        {
            var dynels = DynelManager.AllDynels;
            dynels.RemoveAll(d => (d.Name != "Treasure" && d.Name != "Bottles and Garbage" && d.Name != "Box" && d.Name != "Skeleton" && d.Name != "A Crashed Android" && d.Name != "Broken Machine" && d.Name != "Barrel"));
            return dynels;
        }
        private static Dynel GetClosestLoot()
        {
           var Loot = GetAllLoot();
            Loot.OrderBy(d => d.DistanceFrom(DynelManager.LocalPlayer));
            return Loot.FirstOrDefault();
                            
        }
        private static Dynel GetDownButton()
        {
            Dynel button;
            button = DynelManager.AllDynels.Find(A => A.Name == "Button (down)");
            return button;
        }
        private static Dynel GetBossButton()
        {
            return DynelManager.AllDynels
            .Where(c => (c.Name == "Button (boss)") && c.Room.Floor == DynelManager.LocalPlayer.Room.Floor)
            .FirstOrDefault();
        }

        private static Dynel GetUpButton()
        {
            Dynel button;
            button = DynelManager.AllDynels.Find(A => A.Name == "Button (up)" || A.Name == "Button (boss)");
            return button;
        }
        static bool IsBossRoom()
        {
           if (DynelManager.LocalPlayer.Room.NumDoors == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static void RollMissions()
        {
            if (SMovementController.IsNavigating())
            {
                return;
            }
            else
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(BotNavigator.FindClosestMissionPoint()) > 5)
                    SMovementController.SetNavDestination(BotNavigator.FindClosestMissionPoint());
            }
            if (CurrentTerminal == null)
            {
                CurrentTerminal = GetMissionTerminal();
            }
            if (CurrentTerminal == null)
            {
                Console.WriteLine("Error: No terminal found");
                return;
            }
                if (MissionsRolled)
            {
                return;
            }
            if (CurrentTerminal.DistanceFrom(DynelManager.LocalPlayer) > 5)
            {
                return;
            }
            if (!TerminalUsed)
            {
                CurrentTerminal.Use();
                TerminalUsed = true;
                return;
            }
            if (CurrentTerminal.DistanceFrom(DynelManager.LocalPlayer) > 5)
            {
                return;
            }
            MissionsRolled = true;

            CurrentTerminal.RequestMissions();

            
        }

        static MissionTerminal GetMissionTerminal()
        {
            Dynel term = DynelManager.AllDynels.Find(A => A.Name.Contains("Team Mission Terminal"));
            if (term != null)
            {
                return new MissionTerminal(term);
            }
            else
            {
                Console.WriteLine("Failed to find mission terminal");
                return null;
            }
        }

        static float GetNavPathDistance(Vector3 pos)
        {
            List<Vector3> useless = new List<Vector3>();
            float distance;
            SMovementController.GenerateNavPath(pos, out useless, out distance);
            return distance;
        }
        static Vector3 FindFurtherestPointOnFloor()
        {
            var rooms = Playfield.Rooms;
            rooms.RemoveAll(A => A.Floor != CurrentFloor);
            rooms.OrderByDescending(A => A.Position.DistanceFrom(DynelManager.LocalPlayer.Position));

            if (rooms.First() == null)
            {
                Console.WriteLine("No furtherest point found?");
            }
            return rooms.First().Position;
        }

        static Room GetClosestNeededRoom()
        {
            var rooms = RoomsOfInterest;
            List<Door> doors = new List<Door>();
            foreach (var room in rooms) 
            {
                foreach (var item in room.Doors)
                {
                    doors.Add(item);
                }
            }
            doors.OrderBy(D => D.DistanceFrom(DynelManager.LocalPlayer));
            if (RoomsOfInterest.Contains(doors.First().RoomLink1))
            {
                return doors.First().RoomLink1;
            }
            if (RoomsOfInterest.Contains(doors.First().RoomLink2))
            {
                return doors.First().RoomLink2;
            }
            return null;
        }
    }
}