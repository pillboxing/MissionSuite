﻿using AOSharp.Common.GameData;

namespace AreteRunner
{
    public class Dynels
    {
        public const string PORTAL_TERMINAL = "Exit Arete Landing";
        public const string CREDIT_CARD = "Bank of Rubi-Ka Credit Card";
        public const string QUEST_NPC = "Vaughn Hammond";
    }

    public class Globals
    {
        public static string PLUGIN_DIR;
        public static int NEXT_FREE_INVENTORY_SLOT;
    }

    public class Const
    {
        public const int ARETE_PF = 6553;
        public static int POST_CC_CASH = 15000;
        public static int POST_QUEST_CASH = 1040;
        public static int CREDIT_CARD_ITEM = 297302;
    }

    public class Location
    {
        public Location(Identity identity)
        {

        }
    }
}