﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Common.Unmanaged.Interfaces;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using SharpNav;

namespace AreteRunner
{
    public class BotNavigator
    {
        public static SMovementController movementcontroller;
        internal static KeyWarper _keyWarper;
        private static float TimeSinceLastTick = 0;
        public static bool Loaded;
        public static Dictionary<int, List<Vector3>> SoloTerminals = new Dictionary<int,List<Vector3>>();
        public static Dictionary<int, List<Vector3>> TeamTerminals = new Dictionary<int, List<Vector3>>();
        public static void Initialize()
        {

            SMovementController.Set();
            _keyWarper = new KeyWarper(Globals.PLUGIN_DIR);
            BotAction.Init();
            Network.N3MessageSent += OnN3MessageSent;
            DynelManager.DynelSpawned += OnDynelSpawned;
            Network.N3MessageReceived += OnN3MessageReceived;
            Game.OnUpdate += OnUpdate;
            Game.TeleportEnded += OnTeleportEnded;
            Mission.RollListChanged += RollListChanged;
            SNavMeshGenerator.BakeStatus += OnBakeStatus;

            SMovementController.OnRubberband += OnRubberBand;
            SMovementController.SetStuckLogic(() =>
            {
                    Chat.WriteLine("Trying to handle locked door");
                    var closestdoor = Playfield.Doors.OrderBy(x => x.DistanceFrom(DynelManager.LocalPlayer)).FirstOrDefault();
                    if (closestdoor != null)
                    {
                        var lockpick = Inventory.FindAll("Lock Pick").FirstOrDefault();
                        if (lockpick == null)
                        {
                            Chat.WriteLine("NO LOCKPICK, cannot open door", ChatColor.Red);
                        }
                        else
                        {
                            Item.UseItemOnItem(lockpick.Slot, closestdoor.Identity);
                        }

                    }


            });
            #region MissionTerminals
            SoloTerminals.Add(740, new List<Vector3>());
            TeamTerminals.Add(740, new List<Vector3>());
            SoloTerminals.Add(735, new List<Vector3>());
            TeamTerminals.Add(735, new List<Vector3>());
            TeamTerminals[740].Add(new Vector3(428.7271, 17.39991, 308.3168));
TeamTerminals[740].Add(new Vector3(269.3939, 17.40257, 220.2764));
TeamTerminals[740].Add(new Vector3(271.7616, 17.39982, 285.0982));
TeamTerminals[740].Add(new Vector3(345.1969, 17.3999, 309.9061));
TeamTerminals[740].Add(new Vector3(428.7427, 17.40002, 168.2693));
TeamTerminals[740].Add(new Vector3(271.6138, 17.40018, 342.5843));
TeamTerminals[740].Add(new Vector3(428.7717, 17.40001, 459.7806));
TeamTerminals[740].Add(new Vector3(268.5946, 17.3998, 408.0652));
SoloTerminals[740].Add(new Vector3(428.7521, 17.39997, 319.536));
SoloTerminals[740].Add(new Vector3(266.5611, 17.40202, 220.2412));
SoloTerminals[740].Add(new Vector3(271.746, 17.39994, 287.6998));
SoloTerminals[740].Add(new Vector3(345.086, 17.39989, 319.6578));
SoloTerminals[740].Add(new Vector3(428.7672, 17.40004, 171.7803));
SoloTerminals[740].Add(new Vector3(271.5988, 17.40023, 339.8799));
SoloTerminals[740].Add(new Vector3(428.7668, 17.40001, 456.1641));
SoloTerminals[740].Add(new Vector3(265.799, 17.39979, 408.0782));
TeamTerminals[735].Add(new Vector3(530.1042, 17.4001, 460.185));
TeamTerminals[735].Add(new Vector3(530.129, 17.39988, 316.1632));
TeamTerminals[735].Add(new Vector3(613.9441, 17.40009, 318.8242));
TeamTerminals[735].Add(new Vector3(530.0995, 17.40005, 172.0747));
TeamTerminals[735].Add(new Vector3(691.2868, 17.39948, 220.3869));
TeamTerminals[735].Add(new Vector3(687.2881, 17.4002, 342.3618));
SoloTerminals[735].Add(new Vector3(530.1232, 17.4001, 456.6106));
SoloTerminals[735].Add(new Vector3(530.1487, 17.39991, 312.6351));
SoloTerminals[735].Add(new Vector3(614.0338, 17.40003, 308.6697));
SoloTerminals[735].Add(new Vector3(530.1142, 17.40004, 168.8042));
SoloTerminals[735].Add(new Vector3(687.4305, 17.40019, 288.6326));
            #endregion
            


        }

        private static void OnRubberBand(Vector3 vector)
        {
            if (BotAction.State == "Fighting")
            {
                SMovementController.SetNavDestination(BotAction.Target.Position);
            }
            if (BotAction.State == "Travelling")
            {
                SMovementController.SetNavDestination(BotAction.TargetRoom.Position);
            }
        }

        private static void OnBakeStatus(BakeState state)
        {
            if (BotAction.State == "baking")
            {
                BotAction.State = "NextRoom";
            }    
        }

        private static void OnDynelSpawned(object sender, Dynel e)
        {

        }

        private static void OnN3MessageReceived(object sender, N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity)
                return;
            if (n3Msg is FeedbackMessage feedbackMsg && feedbackMsg.Identity == DynelManager.LocalPlayer.Identity)
            {

                if (feedbackMsg.CategoryId != 0x6E)
                    return;

                if (feedbackMsg.MessageId == 0x0FCA6FF9)
                    _keyWarper.OnWrongMissionKey();
                else if (feedbackMsg.MessageId == 0x0BC6E104)
                    _keyWarper.OnKeyAccepted();
            }
        }

        private static void OnN3MessageSent(object sender, N3Message n3Msg)
        {
            if (n3Msg is GenericCmdMessage genericCmdMsg && n3Msg.Identity == DynelManager.LocalPlayer.Identity && genericCmdMsg.Action == GenericCmdAction.Use)
            {
                Identity dynelId = N3EngineClientAnarchy.TemplateIDToDynelID(genericCmdMsg.Target);

                if (dynelId.Type == IdentityType.MissionKey)
                {
                    _keyWarper.TryEnter(genericCmdMsg.Target, dynelId);
                }

            }
        }

        private static void OnDestinationReached(Vector3 destination)
        {

        }

        private static void RollListChanged(object sender, RollListChangedArgs rollListChanged)
        {
            Chat.WriteLine("New missions!");
            if (BotAction.State != "Rolling")
                return;
            foreach (var item in rollListChanged.MissionDetails)
            {
                
                if (item.Playfield.Instance == Playfield.ModelIdentity.Instance) // && (item.MissionItemData.ToList().RemoveAll(I=>item.Description.Contains("Subconcscious Guidance")) < Team.Members.Count)
                {
                    BotAction.CurrentTerminal = null;
                    BotAction.TerminalUsed = false;
                    Network.Send(new CreateQuestMessage()
                    {
                        MissionId = item.MissionIdentity
                    }) ;
                    Chat.WriteLine("Found mission");
                    BotAction.State = "Initialising";
                    BotAction.MissionsRolled = false;
                }

            }
            BotAction.MissionsRolled = false;
        }

        private static void OnTeleportEnded(object sender, EventArgs e)
        {
            BotAction.PendingTeleport = false;
        }

        public static Vector3 FindClosestMissionPoint()
        {
            var missionpoints = TeamTerminals[Playfield.ModelIdentity.Instance];
            //missionpoints.Add(new Vector3(344.3963, 17.41, 315.06));
            //missionpoints.Add(new Vector3(615.3213, 17.41, 315.4221));
            Vector3 Closest = missionpoints.First();
            foreach (var point in missionpoints)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(point) < DynelManager.LocalPlayer.Position.DistanceFrom(Closest))
                {
                    Closest = point;
                }
            }
            return Closest;
        }

        private static void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning) { return; }

            BotAction.ProcessState();
           //TerminalScan();
        }

        private static void TerminalScan()
        {
            var Terms = DynelManager.AllDynels.FindAll(A => A.Name.Contains("Mission Terminal"));
            foreach (var Term in Terms)
            {
                if (Term.Name.Contains("Team"))
                {
                    foreach (var loc in TeamTerminals[Playfield.ModelIdentity.Instance])
                    {
                        if (loc == Term.Position)
                        {
                            return;
                        }
                    }
                    TeamTerminals[Playfield.ModelIdentity.Instance].Add(Term.Position);
                    Chat.WriteLine("Found team terminal");
                }
                else
                {
                    foreach (var loc in SoloTerminals[Playfield.ModelIdentity.Instance])
                    {
                        if (loc == Term.Position)
                        {
                            return;
                        }
                    }
                    SoloTerminals[Playfield.ModelIdentity.Instance].Add(Term.Position);
                    Chat.WriteLine("Found solo terminal");
                }
            }
        }

        public static NavMeshGenerationSettings NavSettings
        {
            get
            {
                var settings = new NavMeshGenerationSettings();

                settings.CellSize = 0.1f; // Size of a cell in the XZ plane. Smaller values increase detail.
                settings.CellHeight = 0.15f; // Height of a cell. Smaller values increase vertical detail.
                settings.MaxClimb = 0.32f; // Maximum height an agent can climb. Affects obstacle navigation.
                settings.AgentHeight = 1.7f; // Height of the agent. Determines the clearance needed.
                settings.AgentRadius = .85f; // Radius of the agent. Affects how close the agent can get to walls or obstacles.
                settings.MinRegionSize = 8; // Minimum number of cells allowed to form an isolated area (region).
                settings.MergedRegionSize = 20; // Minimum number of cells required to merge small regions into larger ones.
                settings.MaxEdgeLength = 1; // Maximum allowed length for contour edges. Smaller values can improve mesh detail.
                settings.MaxEdgeError = 1.8f; // Maximum allowed deviation for contour simplification. Higher values can simplify geometry.
                settings.ContourFlags = 0;
                settings.VertsPerPoly = 6; // Maximum number of vertices per polygon. Higher values can create more complex shapes.
                settings.SampleDistance = 6; // Sample distance for detail mesh. Lower values create more detailed mesh surfaces.
                settings.MaxSampleError = 1; // Maximum sample error for detail mesh. Controls the accuracy of the detail mesh.
                settings.Bounds = Rect.Default; // Area bounds for the navmesh. Defines the navigation area.
                settings.BuildBoundingVolumeTree = true; // Determines if a bounding volume tree is created for faster queries.
                settings.FilterLargestSection = false;

                //settings.Links = new List<OffMeshConnection>(); // Define connections for paths that go off the mesh (like jump links).

                return settings;
            }
        }
        public static void NavGen()
        {
            SNavMeshGenerator.GenerateAsync(NavSettings).ContinueWith(navMesh =>
            {

                if (navMesh.Result != null)
                {
                    //Chat.WriteLine(filePath);
                    SMovementController.LoadNavmesh(navMesh.Result);

                }
            });
        }


    }

}